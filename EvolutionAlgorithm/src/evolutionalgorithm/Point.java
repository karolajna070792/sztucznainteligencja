package evolutionalgorithm;

public class Point {
    public double x, y;
    public Point(double x, double y){
        this.x = x;
        this.y = y;
    }
    public Point(){
        x = Math.random()*100;
        y = Math.random()*100;
    }
    public double distanceBetween (Point other){
        return Math.sqrt(Math.pow(Math.abs(x - other.x), 2) + Math.pow(Math.abs(y - other.y), 2));
    }
}
