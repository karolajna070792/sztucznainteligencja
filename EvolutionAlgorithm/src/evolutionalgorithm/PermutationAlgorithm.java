package evolutionalgorithm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class PermutationAlgorithm {
       public List<List<Point>> permute(Point...myInts){

        if(myInts.length==1){
            List<Point> arrayList = new ArrayList<Point>();
            arrayList.add(myInts[0]);
            List<List<Point> > listOfList = new ArrayList<List<Point>>();
            listOfList.add(arrayList);
            return listOfList;
        }

        Set<Point> setOf = new HashSet<Point>(Arrays.asList(myInts));   

        List<List<Point>> listOfLists = new ArrayList<List<Point>>();

        for(Point i: myInts){
            ArrayList<Point> arrayList = new ArrayList<Point>();
            arrayList.add(i);

            Set<Point> setOfCopied = new HashSet<Point>();
            setOfCopied.addAll(setOf);
            setOfCopied.remove(i);

            Point[] isttt = new Point[setOfCopied.size()];
            setOfCopied.toArray(isttt);

            List<List<Point>> permute = permute(isttt);
            Iterator<List<Point>> iterator = permute.iterator();
            while (iterator.hasNext()) {
                List<Point> list = iterator.next();
                list.add(i);
                listOfLists.add(list);
            }
        }   

        return listOfLists;
    }
    public double shortestPathLength(List<Point> map){
        List<List<Point>> paths = permute(map.toArray(new Point[map.size()]));
        Chromosome test = new Chromosome();
        test.pointArray = (ArrayList)paths.get(0);
        double result = test.sumOfDistances();
        for(int i = 0; i < paths.size(); i++){
            test.pointArray = (ArrayList)paths.get(i);
            if(test.sumOfDistances() < result)
                result = test.sumOfDistances();
        }
        return result;
    }
}
