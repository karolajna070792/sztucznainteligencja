package evolutionalgorithm;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import org.supercsv.io.CsvListWriter;
import org.supercsv.io.ICsvListWriter;
import org.supercsv.prefs.CsvPreference;
/* reprezentatywne filmiki z Virtual Creature Evolution wysłać
 * wyjaśnić rożnicę między miejscem użycia selecta (początek lub koniec pętli)
 * najgorszy, średni i najlepszy osobnik z generacji
 * dorobić szansę krzyżowania, a brakujące osobniki w nowej generacji dostarczyć ze starej
 * 
 * Problem - znalezienie jak najlepszego rozwiązania (chromosom o najkrótszej sumie odległości pomiędzy punktami - genotypami)
 * Chromosom - ciąg dwuwymiarowych punktów
 * Selekcja - wybór chromosomów oparty o algorytm ruletki z wagami poszczególnych pól (zależnymi od pozycji chromosomu w rankingu danej generacji)
 * Krzyżowanie - losowa mieszanka genów dwóch chromosomów - do zmiany
 * Mutacja - procentowa szansa na wprowadzenie zmiany o zadanej wielkości w kolejności genów danego chromosomu
 * Funkcja przystosowania - suma długości między kolejnymi punktami
 * Warunek zatrzymania algorytmu - ilość iteracji
 */

public class EvolutionAlgorithm {

    ArrayList<Point> map;
    ArrayList<ArrayList> generations;

    public EvolutionAlgorithm(int mapSize, int startingChromosomes) {
        map = new ArrayList<Point>();
        for (int i = 0; i < mapSize; i++) {
            map.add(new Point());
        }
        generations = new ArrayList<ArrayList>();
        ArrayList<Chromosome> firstGeneration = new ArrayList<Chromosome>();
        for (int i = 0; i < startingChromosomes; i++) {
            firstGeneration.add(new Chromosome(map));
        }
        generations.add(sort(firstGeneration));
    }

    public EvolutionAlgorithm(ArrayList<Point> map, int startingChromosomes) {
        this.map = map;
        generations = new ArrayList<ArrayList>();
        ArrayList<Chromosome> firstGeneration = new ArrayList<Chromosome>();
        for (int i = 0; i < startingChromosomes; i++) {
            firstGeneration.add(new Chromosome(map));
        }
        generations.add(sort(firstGeneration));
    }

    public EvolutionAlgorithm(File file, int pointLimit, int startingChromosomes) {
        ArrayList<Point> map = new ArrayList<Point>();
        try {
            Scanner sc = new Scanner(file);
            for (int i = 0; i < 5; i++) {
                sc.nextLine();
            }
            while (map.size() < pointLimit && sc.hasNext()) {
                if (sc.hasNextInt()) {
                    sc.nextInt();
                    map.add(new Point(sc.nextInt(), sc.nextInt()));
                }
                sc.next();
            }
            sc.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.map = map;
        generations = new ArrayList<ArrayList>();
        ArrayList<Chromosome> firstGeneration = new ArrayList<Chromosome>();
        for (int i = 0; i < startingChromosomes; i++) {
            firstGeneration.add(new Chromosome(map));
        }
        generations.add(sort(firstGeneration));
    }

    public ArrayList<Chromosome> sort(ArrayList<Chromosome> generation) {
        ArrayList<Chromosome> result = new ArrayList<Chromosome>();
        if (!generation.isEmpty()) {
            result.add(generation.get(0));
        }
        for (int i = 1; i < generation.size(); i++) {
            for (int j = 0; j < result.size(); j++) {
                if (result.isEmpty() || generation.get(i).sumOfDistances() < result.get(j).sumOfDistances()) {
                    result.add(j, generation.get(i));
                    break;
                }
                if (j == result.size() - 1) {
                    result.add(generation.get(i));
                    break;
                }
            }
        }
        return result;
    }

    public ArrayList<Chromosome> nextGeneration(int selectionSize, double crossoverChance, double mutationChance, int mutationSize) {
        if (mutationChance < 0) {
            mutationChance = 0;
        }
        if (mutationChance > 1) {
            mutationChance = 1;
        }
        if (crossoverChance < 0) {
            crossoverChance = 0;
        }
        if (crossoverChance > 1) {
            crossoverChance = 1;
        }

        ArrayList<Chromosome> result = new ArrayList<Chromosome>();
        ArrayList<Chromosome> selection = selection(generations.get(generations.size() - 1), selectionSize);
        for (int i = 0; i < selection.size(); i += 2) {
            if (Math.random() < crossoverChance) {
                ArrayList<Chromosome> children = new ArrayList<Chromosome>();
                if (i < selection.size() - 1) {
                    children = crossover(selection.get(i), selection.get(i + 1));
                } else {
                    children.add(new Chromosome(selection.get(i)));
                }
                result.addAll(children);
            } else {
                result.add(new Chromosome(selection.get(i)));
                result.add(new Chromosome(selection.get(i + 1)));
            }
        }
        for (int i = 0; i < result.size(); i++) {
            if (Math.random() < mutationChance) {
                result.set(i, mutation(result.get(i), mutationSize));
            }
        }
        return sort(result);
    }

    public ArrayList<Chromosome> selection(ArrayList<Chromosome> generation, int selectionSize) {
        ArrayList<Chromosome> choosingChromosomes = new ArrayList<Chromosome>();
        for (int i = 0; i < generation.size(); i++) {
            for (int j = 0; j < generation.size() - i; j++) {
                choosingChromosomes.add(generation.get(i));
            }
        }
        ArrayList<Chromosome> result = new ArrayList<Chromosome>();
        for (int i = 0; i < selectionSize; i++) {
            result.add(choosingChromosomes.get((int) (Math.random() * choosingChromosomes.size())));
        }
        return result;
    }

    public Chromosome mutation(Chromosome target, int mutations) {
        if (!target.pointArray.isEmpty()) {
            for (int i = 0; i < mutations; i++) {
                target.pointArray.add((int) (Math.random() * target.pointArray.size()), target.pointArray.remove((int) (Math.random() * target.pointArray.size())));
            }
        }
        return target;
    }

    public ArrayList<Chromosome> crossover(Chromosome first, Chromosome second) {
        Chromosome child1 = new Chromosome();
        Chromosome child2 = new Chromosome();
        while (child2.pointArray.size() < first.pointArray.size()) {
            child1.pointArray.add(null);
            child2.pointArray.add(null);
        }
        //int longestPointArrayIndex = 0;
        for (int k = first.pointArray.size() - 1; k >= 0; k--) {
            for (int i = 0; i < first.pointArray.size(); i++) {
                ArrayList<Point> firstPattern = new ArrayList<Point>();
                int iterator = i;
                while (firstPattern.size() < k) {
                    firstPattern.add(first.pointArray.get(iterator));
                    iterator++;
                    if (iterator >= first.pointArray.size()) {
                        iterator = 0;
                    }
                }
                for (int j = 0; j < second.pointArray.size(); j++) {
                    ArrayList<Point> secondPattern = new ArrayList<Point>();
                    iterator = j;
                    while (secondPattern.size() < k) {
                        secondPattern.add(second.pointArray.get(iterator));
                        iterator++;
                        if (iterator >= second.pointArray.size()) {
                            iterator = 0;
                        }
                    }
                    if (firstPattern.equals(secondPattern)) {
                        iterator = i;
                        while (!firstPattern.isEmpty()) {
                            child1.pointArray.set(iterator, firstPattern.remove(0));
                            iterator++;
                            if (iterator >= second.pointArray.size()) {
                                iterator = 0;
                            }
                        }
                        for (iterator = 0; iterator < child1.pointArray.size(); iterator++) {
                            if (child1.pointArray.get(iterator) == null) {
                                for (int it = 0; it < second.pointArray.size(); it++) {
                                    if (!child1.pointArray.contains(second.pointArray.get(it))) {
                                        child1.pointArray.set(iterator, second.pointArray.get(it));
                                    }
                                }
                            }
                        }
                        iterator = j;
                        while (!secondPattern.isEmpty()) {
                            child2.pointArray.set(iterator, secondPattern.remove(0));
                            iterator++;
                            if (iterator >= second.pointArray.size()) {
                                iterator = 0;
                            }
                        }
                        for (iterator = 0; iterator < child2.pointArray.size(); iterator++) {
                            if (child2.pointArray.get(iterator) == null) {
                                for (int it = 0; it < first.pointArray.size(); it++) {
                                    if (!child2.pointArray.contains(first.pointArray.get(it))) {
                                        child2.pointArray.set(iterator, first.pointArray.get(it));
                                    }
                                }
                            }
                        }
                        ArrayList<Chromosome> result = new ArrayList<Chromosome>();
                        result.add(child1);
                        result.add(child2);
                        return result;
                    }
                }
            }
        }
        return new ArrayList<Chromosome>();
    }

    public boolean checkClones(ArrayList<Chromosome> generation) {
        boolean result = false;

        return result;
    }

    public Chromosome bestChromosomeInGeneration(int generation) {
        if (generation >= 0 && generation < generations.size()) {
            return (Chromosome) generations.get(generation).get(0);
        }
        return null;
    }

    public double averagePathInGeneration(int generation) {
        double result = 0;
        if (generation >= 0 && generation < generations.size()) {
            for (int i = 0; i < generations.get(generation).size(); i++) {
                result += ((Chromosome) generations.get(generation).get(i)).sumOfDistances();
            }
            result /= generations.get(generation).size();
        }
        return result;
    }

    private void generateCsvFile(String sFileName, ArrayList<ArrayList> generations) {
        String[][] csvMatrix = new String[generations.size()][4];
        csvMatrix[0][0] = "Generation";
        csvMatrix[0][1] = "Best path length";

        for (int i = 0; i < generations.size(); i++) {
            if (!generations.get(i).isEmpty()) {
                csvMatrix[i][0] = "" + i;
                csvMatrix[i][1] = "" + ((Chromosome) generations.get(i).get(0)).sumOfDistances();
                csvMatrix[i][2] = "" + averagePathInGeneration(i);
                csvMatrix[i][3] = "" + ((Chromosome) generations.get(i).get(generations.get(i).size() - 1)).sumOfDistances();
            }
        }

        ICsvListWriter csvWriter1 = null;
        ICsvListWriter csvWriter2 = null;
        ICsvListWriter csvWriter3 = null;
        try {
            csvWriter1 = new CsvListWriter(new FileWriter("1" + sFileName), CsvPreference.STANDARD_PREFERENCE);
            csvWriter2 = new CsvListWriter(new FileWriter("2" + sFileName), CsvPreference.STANDARD_PREFERENCE);
            csvWriter3 = new CsvListWriter(new FileWriter("3" + sFileName), CsvPreference.STANDARD_PREFERENCE);

            for (int i = 0; i < csvMatrix.length; i++) {
                csvWriter1.write(csvMatrix[i][1]);
                csvWriter2.write(csvMatrix[i][2]);
                csvWriter3.write(csvMatrix[i][3]);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                csvWriter1.close();
                csvWriter2.close();
                csvWriter3.close();
            } catch (IOException e) {
            }
        }

    }

    public static void main(String[] args) {
        ArrayList<Point> map = new ArrayList<Point>();
        map.add(new Point(0, 0));
        map.add(new Point(1, 2));
        map.add(new Point(4, 8));
        map.add(new Point(16, 32));
        map.add(new Point(64, 128));
        map.add(new Point(256, 128));
        map.add(new Point(64, 32));
        map.add(new Point(16, 8));
        map.add(new Point(4, 2));
        map.add(new Point(1, 0));

        final EvolutionAlgorithm evolutionAlgorithm = new EvolutionAlgorithm(new File("a280.tsp"), 50, 10);
        //System.out.println("Best result based on permutation algorithm: "+new PermutationAlgorithm().shortestPathLength(evolutionAlgorithm.map));

        PointsExample ex = new PointsExample(((Chromosome) (evolutionAlgorithm.generations.get(evolutionAlgorithm.generations.size() - 1)).get(0)).pointArray);
        ex.setVisible(true);

        System.out.println("Population size: " + evolutionAlgorithm.generations.get(0).size() + "\nNumber of genes: " + ((Chromosome) (evolutionAlgorithm.generations.get(0).get(0))).pointArray.size());
        for (int i = 0; i < 1000; i++) {
            ex.dpnl.list = ((Chromosome) (evolutionAlgorithm.generations.get(evolutionAlgorithm.generations.size() - 1)).get(0)).pointArray;
            ex.setTitle("Generation " + i);
            ex.setVisible(true);
            ex.repaint();
            ex.revalidate();
            System.out.println("Generation " + (evolutionAlgorithm.generations.size() - 1) + ": " + ((Chromosome) (evolutionAlgorithm.generations.get(evolutionAlgorithm.generations.size() - 1).get(0))).sumOfDistances() + ", " + evolutionAlgorithm.averagePathInGeneration(i) + ", " + ((Chromosome) (evolutionAlgorithm.generations.get(evolutionAlgorithm.generations.size() - 1).get(evolutionAlgorithm.generations.get(evolutionAlgorithm.generations.size() - 1).size() - 1))).sumOfDistances());
            evolutionAlgorithm.generations.add(evolutionAlgorithm.nextGeneration(evolutionAlgorithm.generations.get(0).size(), 0.75, 0.1, 1)); //TODO
        }
        System.out.println("Final generation " + (evolutionAlgorithm.generations.size() - 1) + " best result: " + ((Chromosome) (evolutionAlgorithm.generations.get(evolutionAlgorithm.generations.size() - 1).get(0))).sumOfDistances() + ", average result: " + evolutionAlgorithm.averagePathInGeneration(evolutionAlgorithm.generations.size() - 1) + ", worst result: " + ((Chromosome) (evolutionAlgorithm.generations.get(evolutionAlgorithm.generations.size() - 1).get(evolutionAlgorithm.generations.get(evolutionAlgorithm.generations.size() - 1).size() - 1))).sumOfDistances());
        for (int i = 0; i < evolutionAlgorithm.bestChromosomeInGeneration(evolutionAlgorithm.generations.size() - 1).pointArray.size(); i++) {
            System.out.println("(" + evolutionAlgorithm.bestChromosomeInGeneration(evolutionAlgorithm.generations.size() - 1).pointArray.get(i).x + ", " + evolutionAlgorithm.bestChromosomeInGeneration(evolutionAlgorithm.generations.size() - 1).pointArray.get(i).y + ")");
        }
        evolutionAlgorithm.generateCsvFile("results.csv", evolutionAlgorithm.generations);

    }
}
