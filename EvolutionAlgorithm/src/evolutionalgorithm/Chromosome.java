package evolutionalgorithm;

import java.util.ArrayList;

public class Chromosome {
    public ArrayList<Point> pointArray;
    
    public Chromosome () {
        pointArray = new ArrayList<Point>();
    }
    public Chromosome(ArrayList<Point> map){
        ArrayList<Point> mapCopy = new ArrayList<Point>();
        for(int i = 0; i < map.size(); i++){
            mapCopy.add(map.get(i));
        }
        pointArray = new ArrayList<Point>();
        while(!mapCopy.isEmpty()){
            pointArray.add(mapCopy.remove((int)(Math.random()*mapCopy.size())));
        }
    }
    public Chromosome(Chromosome other){
        pointArray = new ArrayList<Point>();
        for(int i = 0; i < other.pointArray.size(); i++){
            pointArray.add(other.pointArray.get(i));
        }
    }
    public double sumOfDistances(){
        double result = 0;
        for(int i = 0; i < pointArray.size(); i++){
            if(i < pointArray.size()-1){
                result += pointArray.get(i).distanceBetween(pointArray.get(i+1));
            }else{
                result += pointArray.get(i).distanceBetween(pointArray.get(0));
            }
        }
        return result;
    }
}
