package evolutionalgorithm;
import evolutionalgorithm.Point;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class PointsExample extends JFrame {
    DrawPanel dpnl;
    public PointsExample(ArrayList<Point> map) {
        initUI(map);
    }

    public final void initUI(ArrayList<Point> map) {

        dpnl = new DrawPanel(map);
        add(dpnl);

        setSize(500, 500);
        setTitle("Generation 0");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    class DrawPanel extends JPanel {
    public ArrayList<Point> list;
    public DrawPanel(ArrayList<Point> map){
        list = map;
    }
    private void doDrawing(Graphics g) {

        Graphics2D g2d = (Graphics2D) g;

        g2d.setColor(Color.blue);


            Dimension size = getSize();
            Insets insets = getInsets();

            int w = size.width - insets.left - insets.right;
            int h = size.height - insets.top - insets.bottom;

            int[] xPoints = new int[list.size()+1];
            int[] yPoints = new int[list.size()+1];
            for(int j = 0; j < list.size(); j++){
                xPoints[j] = 2*(int)(list.get(j).x) % w;
                yPoints[j] = 2*(int)(list.get(j).y) % h;
            }
            xPoints[list.size()] = 2*(int)(list.get(0).x) % w;
            yPoints[list.size()] = 2*(int)(list.get(0).y) % h;
            g2d.drawPolyline(xPoints, yPoints, list.size()+1);
    }

    @Override
    public void paintComponent(Graphics g) {
        
        super.paintComponent(g);
        doDrawing(g);
    }
}

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                PointsExample ex = new PointsExample(new ArrayList<Point>());
                ex.setVisible(true);
            }
        });
    }
}